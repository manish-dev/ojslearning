
define(['knockout','ojs/ojlabel','ojs/ojinputtext'], function(ko) {
    function AdditionViewModel() {
        var self = this;
        self.num1 = ko.observable(0);
        self.num2 = ko.observable();
        self.num2(0);
        self.addResult = ko.pureComputed(function() {
            return Number(self.num1()) + Number(self.num2());
        });
    }
    return new AdditionViewModel();
})