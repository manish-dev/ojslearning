define(['knockout','models/greetModel' , 'ojs/ojinputtext', 'ojs/ojlabel', 'ojs/ojbutton'], function(ko, model){
    function GreetingViewModel() {
        var self = this;
        self.userName = ko.observable();
        self.userName("manish sharan");
        self.greetingMessage = ko.observable();
        self.processGreeting = function(){
            let message = model.process(self.userName());
            self.greetingMessage(message);
        }
    }
    return new GreetingViewModel();
});