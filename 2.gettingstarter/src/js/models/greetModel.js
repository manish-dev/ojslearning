//require js for modular development

define([], function(){
    function GreetingModel() {
        var self = this;
        self.process = function(userName) {
            return "Greetings "+ userName + "! welcome to Oracle JET";
        }
    }
    return new GreetingModel();
})