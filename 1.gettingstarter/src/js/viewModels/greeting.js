define(['knockout', 'ojs/ojinputtext', 'ojs/ojlabel', 'ojs/ojbutton'], function(ko){
    function GreetingViewModel() {
        var self = this;
        self.userName = ko.observable();
        self.userName("manish sharan");
        self.greetingMessage = ko.observable();
        self.processGreeting = function(){
            self.greetingMessage("Greetings! "+self.userName());
        }
    }
    return new GreetingViewModel();
});